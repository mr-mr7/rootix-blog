<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'rootix-blog' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>B5U/$f8`yGuRxUbCcpo *wH~)v?{TF_S OLY?klBL%<9jdm<S^(!xnBiID0pY,o' );
define( 'SECURE_AUTH_KEY',  '|BU1djR,dryz[:Fv~,)X!~#e>CzyA2FG2&vgr,-FS=1-i_3]WWW<_ot?2y]HLO@#' );
define( 'LOGGED_IN_KEY',    '|YLiHsD`j<BG~DA;ZjEwm_OQ<5aB`-,W=T/0uEX]+xdlIB]I1d>RPd+Gs:KLZQHa' );
define( 'NONCE_KEY',        '`dJ.O1xY`)Z$v!?:SKGoZTNsPX~XaS$R1`8}+CiwPSW%Rq{&+-Ce_oLB40!:oGZ;' );
define( 'AUTH_SALT',        'qzbuGCRchu$ifZS3y,^L)6_2G(cd6(Kjq=bi;Pt0,F.p{w=T8KG3y +qhMvh>7#t' );
define( 'SECURE_AUTH_SALT', 'h~DL1-OMU4kG4%$x| I4ky,8Lyf^k@e;G-P9C)-[O,b/:*AW~Xm&[@vHheWl!]U+' );
define( 'LOGGED_IN_SALT',   '^M(gE.wJla0%(vur!P-:7(6oO6Ru;STolBEXKOw3du/gx={*O#o_uz)qTiH=9xP@' );
define( 'NONCE_SALT',       'O,7p-!B^#LY3TCwlGWki|8:yK)ZV[?FE$|v_RA*(tmy#brp@9Cw<f|<)ZZ,d>+ID' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'RB_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
