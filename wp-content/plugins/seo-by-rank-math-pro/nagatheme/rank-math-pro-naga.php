<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

// Rank Math Pro License
if ( ! class_exists( 'Rank_Math_Pro_Zhk_SDK' ) ) {
	require_once 'includes/class-rank-math-pro-license.php';
}

// Copy Translations As Content --- seo-by-rank-math
add_action( 'admin_init', 'rank_math_naga_copy_translations' );
function rank_math_naga_copy_translations() {
	if ( is_rtl() ) {
		$locale = 'fa_IR';
		$domain = 'seo-by-rank-math';
		$mofile = $domain . '-' . $locale . '.mo';
		$file1 = plugin_dir_path( __FILE__ ) . 'languages/' . $mofile;
		$file2 = WP_LANG_DIR . '/plugins/' . $mofile;
		$contentx =@ file_get_contents( $file1 );
		$openedfile = fopen( $file2, "w" );
		fwrite( $openedfile, $contentx ) ;
		fclose( $openedfile );
		if ( $contentx !== FALSE ) {
			//var_dump("Text-Domain: $domain - Translations Has Been Copied Successfully!");
		}
	}
}

// Load Translations --- rank-math-pro | rank-math
add_action( 'init', 'rank_math_naga_load_translations' );
function rank_math_naga_load_translations() {
	if ( is_rtl() ) {
		$locale = 'fa_IR';
		$domains = array ('rank-math', 'rank-math-pro');
		foreach ( $domains as $domain ) {
			$mofile = $domain . '-' . $locale . '.mo';
			$path = plugin_dir_path( __FILE__ ) . 'languages/' . $mofile;
			unload_textdomain( $domain );
			load_textdomain( $domain, $path );
		}
	}
}

// Fixing RTL Admin Styles
add_action( 'admin_enqueue_scripts', 'rank_math_naga_admin_css' );
function rank_math_naga_admin_css() {
	if( Rank_Math_Pro_Zhk_SDK::is_activated() === true ) {
		if ( is_rtl() ) {
			wp_enqueue_style( 'rank-math-naga-admin-css', plugin_dir_url( __FILE__ ) . 'css/rank-math-naga-admin.css' );
		}
	}
}

// Fixing Front-End Styles
// add_action( 'wp_enqueue_scripts', 'rank_math_naga_front_css' );
function rank_math_naga_front_css() {
	if( Rank_Math_Pro_Zhk_SDK::is_activated() === true ) {
		if ( is_rtl() ) {
			wp_enqueue_style( 'rank-math-naga-front-css', plugin_dir_url( __FILE__ ) . 'css/rank-math-naga-front.css' );
		}
	}
}

// Disable Options If License is not Valid
add_action( 'admin_init', 'rank_math_pro_naga_disable_options' );
function rank_math_pro_naga_disable_options() {
	if( Rank_Math_Pro_Zhk_SDK::is_activated() !== true ) {
		remove_menu_page( 'rank-math' );				  //RankMath
	}
}


// Add NagaTheme Help Sub-Menu Page
add_action( 'admin_menu', 'rank_math_pro_naga_submenu_kb', 100 );
function rank_math_pro_naga_submenu_kb() {
	if ( is_rtl() ) {
		add_submenu_page( 
			'rank-math',
			__('NagaTheme KB', 'rank-math-pro'),
			__('NagaTheme KB', 'rank-math-pro'),
			'manage_options',
			'nagatheme-knowledge-base',
			'rank_math_pro_naga_kb_page'
		);
	}
}
function rank_math_pro_naga_kb_page() {
	if ( is_rtl() ) {
		require_once('includes/nagatheme-kb.php');
	}
}



// End of The Game...