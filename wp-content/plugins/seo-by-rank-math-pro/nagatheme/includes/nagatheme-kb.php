<!DOCTYPE html>
<html>
<head>
	<style>
		.naga-box-help {
			position: relative;
			max-width: 100%;
			margin: 0 20px;
		}
		.naga-box {
			position: relative;
			margin: 20px 0;
			background: #fff;
			border: 1px solid #b5bfc9;
			border-radius: 5px;
			padding: 25px;
		}
		.naga-box-content {
			font-size: 15px;
			color: #424242;
		}
		.naga-list-icon li {
			padding: 0 0 20px 0;
		}
		.naga-list-icon a {
			text-decoration: none;
		}
		.naga-list-icon div {
			padding: 0 65px 0 0;
		}
		.naga-list-icon p {
			margin: 10px 0;
			color: #424242;
			text-align: justify;
		}
		.naga-list-icon li:nth-child(odd) i {
			font-size: 35px;
			color: #F5F5F5;
			background: #E64A19;
		}
		.naga-list-icon li:nth-child(even) i {
			font-size: 35px;
			color: #F5F5F5;
			background: #3F51B5;
		}
		.naga-list-icon li i {
			font-size: 20px;
			line-height: 50px;
			display: inline-block;
			color: #ccc;
			border-radius: 5px;
			background: #eee;
			height: 50px;
			width: 50px;
			float: right;
			margin: 0;
			vertical-align: middle;
		}
		.naga-box h3 {
			margin: 0 0 20px 0;
		}
		
		#nagatb {
			border-collapse: collapse;
			width: 100%;
		}
		#nagatb td, #nagatb th {
			border: 1px solid #ddd;
			padding: 8px;
			text-align: center;
		}
		#nagatb tr:nth-child(even){background-color: #f2f2f2;}
		#nagatb tr:hover {background-color: #ddd;}
		#nagatb th {
			padding-top: 12px;
			padding-bottom: 12px;
			background-color: #4CAF50;
			color: white;
		}
	</style>
</head>
<body>
	<?php
	$naga_links = [];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/m20UQ',
		'title'    => esc_html__( 'Rank Math Titles and Meta Settings', 'rank-math-pro' ),
		'desc'     => esc_html__( 'In this knowledge base tutorial, we are going to cover the Tiles and Meta settings in Rank Math.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-editor-paste-text',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/m20UQ',
		'title'    => esc_html__( 'Rank Math Local SEO Settings', 'rank-math-pro' ),
		'desc'     => esc_html__( 'The Local SEO section contains all the settings related to Local Businesses.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-location',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/ABWCg',
		'title'    => esc_html__( 'Configuring Schema Markup in Rank Math (Part 1)', 'rank-math-pro' ),
		'desc'     => esc_html__( 'This is the first part of Schema Markup. we will explain the Schema options in Rank Math and how you can use them to add schema to your website.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-awards',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/RS4qe',
		'title'    => esc_html__( 'Configuring Schema Markup in Rank Math (Part 2)', 'rank-math-pro' ),
		'desc'     => esc_html__( 'This is the second part of Schema Markup. we will explain the Schema options in Rank Math and how you can use them to add schema to your website.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-editor-code',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/HqJj0',
		'title'    => esc_html__( 'How to Set Up Redirections?', 'rank-math-pro' ),
		'desc'     => esc_html__( 'HTTP redirects are incredibly important in SEO & UX. When you visit a URL with a redirection, your browser automatically jumps to a new address.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-share-alt2',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/f1Dvd',
		'title'    => esc_html__( 'Product Schema For WooCommerce', 'rank-math-pro' ),
		'desc'     => esc_html__( 'In this tutorial, we’ll walk you through how to use the Product Schema type on products you create using the WooCommerce.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-store',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/wJbSl',
		'title'    => esc_html__( 'Google Search Console and Analytics', 'rank-math-pro' ),
		'desc'     => esc_html__( 'It is the time to take a deep look at the search console and google analytics and use them with rank math pro correctly.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-google',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/K0dP5',
		'title'    => esc_html__( 'RankMath Analytics Panel', 'rank-math-pro' ),
		'desc'     => esc_html__( 'Now we are going to learn how to use the rank math analytics panel with a complete tutorial.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-analytics',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/2xrF6',
		'title'    => esc_html__( 'How to connect Rank Math to Google Analytics?', 'rank-math-pro' ),
		'desc'     => esc_html__( 'Rank Math’s goal is to make on-page SEO easy. With that said, Rank Math’s built-in Analytics module makes it easier than ever to install Google Analytics on your website.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-chart-bar',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/Fo79C',
		'title'    => esc_html__( 'FAQ Schema Behavior in SERPs', 'rank-math-pro' ),
		'desc'     => esc_html__( 'According to questions from many users we published a new complete article to describe FAQ schema behavior in SERPs.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-clipboard',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/khIwq',
		'title'    => esc_html__( 'Speakable Schema with Rank Math', 'rank-math-pro' ),
		'desc'     => esc_html__( 'In this tutorial, we’ll walk you through how to use Rank Math Pro to optimize an article for voice searchs for SERPs.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-microphone',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/r5KCV',
		'title'    => esc_html__( 'PodcastEpisode Schema with Rank Math', 'rank-math-pro' ),
		'desc'     => esc_html__( 'In this tutorial, we’ll walk you through how to use Rank Math Pro to optimize a PodcastEpisodein in an article for SERPs.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-embed-audio',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/OQY7s',
		'title'    => esc_html__( 'Self-serving reviews', 'rank-math-pro' ),
		'desc'     => esc_html__( 'In this tutorial, we’ll walk you through what is self-serving reviews and how to avoid from it.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-format-chat',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/uX06P',
		'title'    => esc_html__( 'Fix Sitemap Post and Product tags', 'rank-math-pro' ),
		'desc'     => esc_html__( 'In this tutorial, we’ll walk you through solving issues about sitemap post and product tags.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-location-alt',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/Cau1q',
		'title'    => esc_html__( 'Either “offers”, “review”, or “aggregateRating” should be specified', 'rank-math-pro' ),
		'desc'     => esc_html__( 'If you have come across an error: Either “offers”, “review”, or “aggregateRating” should be specified in the Google Search Console, you’ve come to the right place.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-star-half',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/TQSvX',
		'title'    => esc_html__( 'Fix $title variable required Issue', 'rank-math-pro' ),
		'desc'     => esc_html__( 'In this tutorial, we’ll walk you through how to solve $title variable required issue.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-lock',
	];
	
	$naga_links[] = [
		'url'    => 'https://vrgl.ir/tnVvn',
		'title'    => esc_html__( 'Fix Rank Math Issue with Solar Date', 'rank-math-pro' ),
		'desc'     => esc_html__( 'In this tutorial, we’ll walk you through how to solve solar date issues occurred with Rank Math.', 'rank-math-pro' ),
		'icon'     => 'dashicons dashicons-calendar-alt',
	];

	?>
	<div class="naga-box-help">

		<div class="naga-box">

			<header>
				<h3><?php esc_html_e( 'NagaTheme Persian Knowledge Base', 'rank-math-pro' ); ?></h3>
				<p><?php esc_html_e( 'Here you can find the most popular documentations about Rank Math. Read them and make your knowledge be upgrated.', 'rank-math-pro' ); ?></p>
				<br>
			</header>

			<div class="naga-box-content">

				<ul class="naga-list-icon">

					<?php foreach( $naga_links as $naga_link ) { ?>
					
						<li>
							<i class="<?php echo $naga_link['icon']; ?>"></i>
							<div>
								<strong><a href="<?php echo $naga_link['url']; ?>" target="_blank"><?php echo $naga_link['title']; ?></a></strong>
								<p><?php echo $naga_link['desc']; ?></p>
							</div>

						</li>
					
					<?php } ?>

				</ul>
				
				<!--
				<a class="button button-secondary button-xlarge" href="#" target="_blank"><?php esc_html_e( 'Visit NagaTheme Knowledge Base', 'rank-math-pro' ); ?></a>
				-->
				
			</div>

		</div>
		
	</div><!--.two-col-->


	<?php
	// Items of The Translation Table
	$naga_trans_items = array(
		'Title'		=>	'عنوان',
		'Alt'		=>	'متن جایگزین',
		'Schema'	=>	'اسکیما - طرحواره',
		'Sitemap'	=>	'نقشه سایت',
		'Post Type'	=>	'پست تایپ',
		'Warning'	=>	'هشدار',
		'Error'		=>	'خطا',
		'Taxonomy'	=>	'طبقه بندی',
		'Category'	=>	'دسته بندی',
		// Copy Above Line and Add Here
		'Breadcrumbs'	=>	'مسیر راهنما'
	);
	// Let's sort the srings alphabetically
	ksort($naga_trans_items);
	?>
	
	<div class="naga-box-help">
	
		<div class="naga-box">
		
			<header>
				<h3>راهنمای ترجمه‌های ناگاتم در رنک مث ژاکت</h3>
				<p>در این قسمت شما می‌توانید لیست مهمترین اصطلاحات ترجمه شده در رنک مث ژاکت را مشاهده کنید.  ترتیب جدول بر اساس حروف الفبای انگلیسی می‌باشد.</p>
				<br>
			</header>
			
			<table id="nagatb">
				<tr>
					<th>لیست کلمات</th>
					<th>ترجمه ها</th>
				</tr>
				<?php foreach( $naga_trans_items as $key => $value ) { ?>
					<tr>
						<td><?php echo "{$key}"; ?></td>
						<td><?php echo "{$value}"; ?></td>
					</tr>
				<?php } ?>
			</table>
			
			<br><p>لازم به ذکر است که هنوز برخی از ترجمه‌های این افزونه از سمت مخزن وردپرس فراخوانی می‌شوند و نیاز به اصلاح و ترجمه مجدد دارند که در حال اصلاح و بهبود آن‌ها هستیم. از حمایت شما قدردانیم.</p>
			
		</div>
		
	</div><!--.two-col-->
	
</body>
</html>
