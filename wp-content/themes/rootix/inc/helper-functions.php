<?php
function rootix_callback_commentsHTML5($comment, $args, $depth)
{
    $comment = $comment;
    $id = $comment->comment_ID;
    $author = $comment->comment_author;
    $content = $comment->comment_content;
    $children_comment = $comment->get_children();
    ?>
    <div class="comment">
    <?php echo get_avatar($post->post_author, '', '', $author, ['class' => 'comment__profile'], '') ?>

    <div class="comment__info-container">
        <div class="comment__info">
            <div class="comment__desc">
                <div class="comment__title">
                    <div class="comment__name-container">
                        <h3 class="comment__writer"><?php echo $author; ?></h3>

                        <!--<div class="comment__stars">
                            <i class="fa fa-star comment__star"></i>
                            <i class="fa fa-star comment__star active"></i>
                            <i class="fa fa-star comment__star active"></i>
                            <i class="fa fa-star comment__star active"></i>
                            <i class="fa fa-star comment__star active"></i>
                        </div>-->
                    </div>
                    <span class="comment__calender">در تاریخ <?php echo the_date('d M Y'); ?></span>
                </div>
            </div>

            <div class="comment__edit reply">
                <?php
                $img_src= IMAGES_DIR . 'single-page/reply-icon.svg';
                comment_reply_link(array_merge($args,[
                        'depth' => $depth,
                        'max_depth' => $args['max_depth'],
//                        'reply_text' => "<img src='$img_src'>",
                    ]));
//                ?>
            </div>
        </div>
        <p class="comment__text"><?php echo $content; ?></p>
        <?php if (count($children_comment) > 0): ?>
            <div class="other-comments">
                <?php foreach ($children_comment as $item): ?>
                    <div class="other-comments__comment">
                        <!--<img src="<?php /*echo get_avatar_url($item->comment_ID, '70'); */ ?>"
                                alt="<?php /*echo $item->comment_author; */ ?>"
                                class="other-comments__image"/>-->
                        <?php echo get_avatar($post->post_author, '', '', $item->comment_author, ['class' => 'other-comments__image'], '') ?>


                        <div class="comment__title other-comments__title">
                            <h3 class="comment__writer other-comments__writer">
                                <?php echo $item->comment_author; ?>
                            </h3>

                            <span class="comment__calender other-comments__calender">در تاریخ <?php the_date(); ?></span>

                            <p class="other-comments__text"><?php echo $item->comment_content; ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

    <!-- according to codex don't close the last tag -->
<?php }

function rootix_end_callback_commentsHTML5()
{
    echo '</div><hr class="comment__line" />';
}

function wpb_move_comment_field_to_bottom($fields)
{
    $comment_field = $fields['comment'];
    unset($fields['comment']);
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter('comment_form_fields', 'wpb_move_comment_field_to_bottom');