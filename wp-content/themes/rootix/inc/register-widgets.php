<?php
function rootix_widgets_init()
{
    // Desktop Footer Widget
    register_sidebar([
        'name' => 'فوتر دکستاپ',
        'id' => 'footer-blog',
        'before_widget' => '<div class="col-12 col-sm-6 col-lg-3"><div class="footer-contact">',
        'after_widget' => '</div></div>',
        'before_title' => '<h3 class="footer-title">',
        'after_title' => '</h3>',
    ]);
    //## Desktop Footer Widget

    // Desktop Footer Widget
    register_sidebar([
        'name' => 'فوتر موبایل',
        'id' => 'footer-blog-mobile',
        'before_widget' => '<div class="col-6 col-lg-3 footer-item"><div class="footer-contact">',
        'after_widget' => '</div></div>',
        'before_title' => '<h3 class="footer-title">',
        'after_title' => '</h3>',
    ]);
    //## Desktop Footer Widget

}

add_action('widgets_init', 'rootix_widgets_init');