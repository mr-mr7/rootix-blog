const categoryBtn = document.querySelector(".blog-res-filters-title-btn");
const categoryDrppdownItems = [
  ...document.querySelectorAll(".categories li button"),
];

categoryBtn.addEventListener("click", () => {
  const categoryDrppdown = document.querySelector(".category-drppdown");
  categoryDrppdown.classList.toggle("active");

  const svgIcon = categoryBtn.querySelector("svg");

  const categoryIcon = document.querySelector(".category-icon");
  let newSvgIcon;
  if (svgIcon.classList.contains("plus")) {
    newSvgIcon = `<svg class="minus" xmlns="http://www.w3.org/2000/svg" width="18.881" height="18.881" viewBox="0 0 18.881 18.881"><g transform="translate(-11582.482 -4146.374)"><path d="M9.441,17.7A8.261,8.261,0,1,0,1.18,9.441,8.261,8.261,0,0,0,9.441,17.7Zm0,1.18A9.441,9.441,0,1,0,0,9.441,9.441,9.441,0,0,0,9.441,18.881Z" transform="translate(11582.482 4146.374)" fill="#404040" fill-rule="evenodd"/><line x2="11" transform="translate(11586.5 4155.5)" fill="none" stroke="#000" stroke-width="1"/></g></svg>`;
    categoryIcon.innerHTML = newSvgIcon;
  } else if (svgIcon.classList.contains("minus")) {
    newSvgIcon = `                <svg
    xmlns="http://www.w3.org/2000/svg"
    width="18.881"
    class="plus"
    height="18.881"
    viewBox="0 0 18.881 18.881"
  >
    <g
      id="Group_1518"
      data-name="Group 1518"
      transform="translate(-354.846 -594.737)"
    >
      <path
        id="Path_3041"
        data-name="Path 3041"
        d="M13.185,7.875a.59.59,0,0,1,.59.59v4.72a.59.59,0,0,1-.59.59H8.465a.59.59,0,1,1,0-1.18H12.6V8.465A.59.59,0,0,1,13.185,7.875Z"
        transform="translate(351.101 590.992)"
        fill="#404040"
        fill-rule="evenodd"
      />
      <path
        id="Path_3042"
        data-name="Path 3042"
        d="M16.875,17.465a.59.59,0,0,1,.59-.59h4.72a.59.59,0,0,1,0,1.18h-4.13v4.13a.59.59,0,1,1-1.18,0Z"
        transform="translate(346.821 586.713)"
        fill="#404040"
        fill-rule="evenodd"
      />
      <path
        id="Path_3043"
        data-name="Path 3043"
        d="M9.441,17.7A8.261,8.261,0,1,0,1.18,9.441,8.261,8.261,0,0,0,9.441,17.7Zm0,1.18A9.441,9.441,0,1,0,0,9.441,9.441,9.441,0,0,0,9.441,18.881Z"
        transform="translate(354.846 594.737)"
        fill="#404040"
        fill-rule="evenodd"
      />
    </g>
  </svg>`;
    categoryIcon.innerHTML = newSvgIcon;
  }
});

categoryDrppdownItems.forEach((categoryDrppdownItem, index) => {
  categoryDrppdownItem.addEventListener("click", () => {
    const currentCategory = categoryDrppdownItems[index].textContent;
    const categoryBtnTitle = categoryBtn.querySelector("span");
    categoryBtnTitle.innerHTML = currentCategory;
  });
});
