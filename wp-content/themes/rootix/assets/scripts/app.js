const menuIcon = document.querySelector(".res-menu__bar-icon");
const menu = document.querySelector(".container-menu ul");

menuIcon.addEventListener("click", (e) => {
  menu.classList.toggle("active");
});

document.addEventListener("click", (evt) => {
  let targetElement = evt.target; // clicked element

  do {
    if (targetElement == menu || targetElement == menuIcon) {
      return;
    }

    targetElement = targetElement.parentNode;
  } while (targetElement);

  menu.classList.remove("active");
});
