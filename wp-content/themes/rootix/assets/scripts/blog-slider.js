$(document).ready(function () {
    $(".blog-page-top-slide").owlCarousel({
        // loop:true,
        rtl: true,
        margin: 15,
        responsiveClass: true,
        responsive: {
            0: {
                dots: true,
                items: 1,
                nav: false,
            },
        },
    });
})
  