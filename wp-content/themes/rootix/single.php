<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>
<?php while (have_posts()): the_post(); ?>
    <!-- Start Main Content -->
    <main>
        <!-- Start Section One -->
        <section class="section-1">
            <div class="container-secondary container-secondary-single-page">
                <div class="row">
                    <div class="col-12">
                        <div class="singlePage-sec-1">
                            <div class="singlePage-sec-one-img">
                                <img src="<?php the_post_thumbnail_url('medium_large'); ?>" alt="<?php the_title() ?>">
                            </div>
                            <h1>
                                <?php the_title() ?>
                            </h1>
                            <div class="d-flex category-blog-title">
                                <h5><?php the_category() ?></h5>
                                <span dir="rtl"><?php the_modified_date('Y M d') ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section One -->

        <!-- Start Section Two -->
        <section class="section-2">
            <div class="container-secondary container-secondary-single-page">
                <div class="row">
                    <div class="col-12">
                        <div class="single-page-paragraph">
                            <?php the_content(); ?>
                        </div>
                        <div class="d-flex items-end single-page-tags-container">
                            <div class="single-page-tags">
                                <?php the_tags('<span class="single-page-tags-title">تگ:</span>', ' ', ''); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="single-page-line" style="margin-top: 1rem">
            </div>
        </section>
        <!-- End Section Two -->


        <?php $prev_post = get_previous_post();
        $next_post = get_next_post(); ?>
        <!-- Start Section Five -->
        <section>
            <div class="container-secondary container-secondary-single-page">

                <div class="d-flex items-end justify-between comment-desc-sec-container">
                    <div class="comment-desc-3">
                        <a href="<?php echo get_permalink($next_post) ?>">
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="13"
                                    height="9"
                                    viewBox="0 0 15.455 9.774"
                            >
                                <path
                                        id="Path_141"
                                        data-name="Path 141"
                                        d="M0,94.862a.543.543,0,0,0,.543.543H13.618l-3.425,3.422a.541.541,0,0,0,.767.764L15.3,95.244a.561.561,0,0,0,0-.767l-4.347-4.344a.542.542,0,0,0-.767.767l3.422,3.422H.54A.536.536,0,0,0,0,94.862Z"
                                        transform="translate(0 -89.975)"
                                />
                            </svg>
                            بعدی
                        </a>
                        <p><?php echo $next_post->post_title ?></p>
                    </div>

                    <div class="vertical-line-single-page"></div>

                    <div class="comment-desc-4">
                        <a href="<?php echo get_permalink($prev_post) ?>">
                            قبلی
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="13"
                                    height="9"
                                    viewBox="0 0 15.455 9.774"
                            >
                                <path
                                        id="Path_141"
                                        data-name="Path 141"
                                        d="M0,94.862a.543.543,0,0,0,.543.543H13.618l-3.425,3.422a.541.541,0,0,0,.767.764L15.3,95.244a.561.561,0,0,0,0-.767l-4.347-4.344a.542.542,0,0,0-.767.767l3.422,3.422H.54A.536.536,0,0,0,0,94.862Z"
                                        transform="translate(0 -89.975)"
                                />
                            </svg>
                        </a>
                        <p><?php echo $prev_post->post_title ?></p>
                    </div>
                </div>

                <div class="single-page__user">
                    <a href="<?php the_author_meta('url') ?>">
                        <?php echo get_avatar($post->post_author, '', '', '', ['class' => 'single-page__user-profile'], '') ?>
                    </a>
                    <div class="single-page__user-title">
                        <h3><a href="<?php the_author_meta('url') ?>"><?php the_author() ?></a> </h3>
                        <p>
                            <?php the_author_meta('description'); ?>
                        </p>
                    </div>
                </div>

                <hr class="single-page-line single-page-line-2"/>
            </div>
        </section>
        <!-- End Section Five -->


        <!-- Start Comments -->


        <?php
        comments_template();
        ?>

        <!-- End Comments -->
    </main>
    <!-- End Main Content -->
<?php endwhile; ?>

<?php get_footer(); ?>
