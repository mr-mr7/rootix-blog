<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password,
 * return early without loading the comments.
 */
if (post_password_required()) {
    return;
}
?>

<section class="section-5">
    <div class="single-page-application-container">
        <div class="single-page-comments about-comments">
            <div class="comment-container <?php echo get_option('show_avatars') ? 'show-avatars' : ''; ?>">
                <?php $comments = get_comments([
                    'post_id' => $post->ID,
                    'parent' => 0,
                    'status' => get_option('comment_moderation'),
                ]);
                if (count($comments) > 0) :
                    wp_list_comments(
                        array(
                            'style' => 'div',
                            'callback' => 'rootix_callback_commentsHTML5',
                            'end-callback' => 'rootix_end_callback_commentsHTML5',
                            'format' => 'html5', // or 'xhtml' if no 'HTML5' theme support
                        ), $comments);
                    ?>
                    <!-- .comment-list -->
                <?php else: ?>
                    <div class="alert alert-info">اولین کامنت را شما بگذارید</div>
                <?php endif; ?>
            </div>
        </div>
        <?php
        comment_form([
            //Define Fields
            'fields' => array(
                //Author field
                'author' => '<input name="author" type="text" placeholder="نام و نام خانوادگی" />',
                //Email Field
                'email' => '<input name="email" type="text" placeholder="پست الکترونیک" />',
                //Cookie Field
                'cookies' => '',
            ),
            'comment_field' => '<textarea name="comment" placeholder="متن پیام" class="comment__input"></textarea>',
            'class_container' => 'single-page-submit-comments about-comments',
            'comment_notes_before' => '',
            'comment_notes_after' => '',
        ]); ?>
    </div>
</section>
