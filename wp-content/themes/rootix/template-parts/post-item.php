<div class="main-news">
    <a href="<?php the_permalink(); ?>" class="main-news__image">
        <img src="<?php the_post_thumbnail_url('medium'); ?>" alt="<?php the_title() ?>"/>
    </a>

    <div class="main-news__info">
        <div class="main-news__writer">
            <span><?php the_author() ?></span>
            <span><?php comments_number('بدون نظر', '1 نظر', '% نظر'); ?></span>
        </div>

        <a href="<?php the_permalink(); ?>" class="main-news__title">
            <?php the_title() ?>
        </a>

        <p class="main-news__desc">
            <?php echo get_the_excerpt() ?>
        </p>

        <a href="<?php the_permalink(); ?>" class="main-news__btn"> بیشتر بخوانید </a>
    </div>
</div>