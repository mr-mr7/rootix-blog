<?php extract($args); ?>
<div class="side-news-container__side-news">
    <a href="<?php echo get_the_permalink($item) ?>" class="side-news-container__image">
        <img src="<?php echo get_the_post_thumbnail_url($item,'sidebar-small'); ?>" alt="<?php echo $item->post_title; ?>" />
    </a>
    <div class="side-news">
        <h4><a style="color: #1d2a3b" href="<?php echo get_the_permalink($item) ?>"><?php echo $item->post_title ?></a> </h4>
        <span><?php echo get_the_modified_time('Y M d',$item) ?></span>
    </div>
</div>