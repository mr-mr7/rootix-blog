<?php
/**
 * A template partial to output pagination for the Twenty Twenty default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$prev_text = sprintf(
	'<button class="pagination-page">
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="7.645"
            height="12.38"
            viewBox="0 0 7.645 12.38">
            <path
                id="Icon_material-expand-more"
                data-name="Icon material-expand-more"
                d="M19.926,12.885,15.19,17.61l-4.735-4.725L9,14.34l6.19,6.19,6.19-6.19Z"
                transform="translate(-12.885 21.38) rotate(-90)"
                opacity="0.6"
            />
        </svg>
    </button>',
	''
);
$next_text = sprintf(
    '<button class="pagination-page">
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="7.645"
            height="12.38"
            viewBox="0 0 7.645 12.38">
            <path
                id="Icon_material-expand-more"
                data-name="Icon material-expand-more"
                d="M19.926,12.885,15.19,17.61l-4.735-4.725L9,14.34l6.19,6.19,6.19-6.19Z"
                transform="translate(-12.885 21.38) rotate(-90)"
                opacity="0.6"
            />
        </svg>
    </button class="pagination-page">',
    ''
);

$posts_pagination = get_the_posts_pagination(
	array(
		'mid_size'  => 1,
		'prev_text' => $prev_text,
		'next_text' => $next_text,
        'before_page_number' => '<button class="pagination-page">',
        'after_page_number' => '</button>',
        'class' => 'pagination'
	)
);

// If we're not outputting the previous page link, prepend a placeholder with `visibility: hidden` to take its place.
if ( strpos( $posts_pagination, 'prev page-numbers' ) === false ) {
	$posts_pagination = str_replace( '<div class="nav-links">', '<div class="nav-links"><span class="prev page-numbers placeholder" aria-hidden="true">' . $prev_text . '</span>', $posts_pagination );
}

// If we're not outputting the next page link, append a placeholder with `visibility: hidden` to take its place.
if ( strpos( $posts_pagination, 'next page-numbers' ) === false ) {
	$posts_pagination = str_replace( '</div>', '<span class="next page-numbers placeholder" aria-hidden="true">' . $next_text . '</span></div>', $posts_pagination );
}

if ( $posts_pagination ) { ?>

	<div class="pagination-wrapper section-inner">

		<?php echo $posts_pagination; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- already escaped during generation. ?>

	</div><!-- .pagination-wrapper -->

	<?php
}
