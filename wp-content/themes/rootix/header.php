<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
wp_body_open();
?>

<!-- Start Header -->
<header class="blog-page-header">
    <div class="container">
        <nav class="nav blog-nav blog__responsive-nav res-menu">
            <div>
                <a href="/" class="logo">
                    <img src="<?php echo IMAGES_DIR?>logo.png" alt="لوگو" />
                </a>
                <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
            </div>

            <div class="login-container">
                <!--<a href="#" class="language">
                    <i class="fas fa-angle-down"></i>
                    <p>EN</p>
                    <div>
                        <img src="<?php echo IMAGES_DIR?>flag.png" alt="پرچم" />
                    </div>
                </a>-->
                <a href="https://rootix.io/auth/login" class="login-btn">
                    <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="18"
                            height="9"
                            viewBox="0 0 18 9"
                    >
                        <image
                                id="icons8-long_arrow_up_filled"
                                width="18"
                                height="9"
                                xlink:href="<?php echo IMAGES_DIR?>login-right-arrow.png"
                        />
                    </svg>
                    ورود
                </a>
            </div>
        </nav>
    </div>
</header>
<!-- End Header -->

<!-- responsive menu -->
<header class="responsive-blog-menu">
    <div class="container">
        <div class="res-menu">
            <div class="res-menu__top res-menu__top-blog">
                <button class="res-menu__bar-icon">
                    <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="30"
                            height="21"
                            class="res-menu__bars"
                            viewBox="0 0 30 21"
                    >
                        <g
                                id="Icon_feather-menu"
                                data-name="Icon feather-menu"
                                transform="translate(-3 -7.5)"
                        >
                            <path
                                    id="Path_8025"
                                    data-name="Path 8025"
                                    d="M4.5,18h27"
                                    fill="none"
                                    stroke="#000"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="3"
                            />
                            <path
                                    class="path-menu-icon"
                                    id="Path_8026"
                                    data-name="Path 8026"
                                    d="M4.5,9h27"
                                    fill="none"
                                    stroke="#000"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="3"
                            />
                            <path
                                    id="Path_8027"
                                    data-name="Path 8027"
                                    d="M4.5,27h27"
                                    fill="none"
                                    stroke="#000"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="3"
                            />
                        </g>
                    </svg>
                </button>

                <a href="/">
                    <img src="<?php echo IMAGES_DIR?>logo.png" alt="لوگو" class="res-menu__logo" />
                </a>

                <button class="res-menu__question-mark">
                    <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="30"
                            height="30"
                            viewBox="0 0 30 30"
                    >
                        <g
                                id="Group_1380"
                                data-name="Group 1380"
                                transform="translate(-40 -13)"
                        >
                            <g
                                    id="Rectangle_1098"
                                    data-name="Rectangle 1098"
                                    transform="translate(40 13)"
                                    fill="none"
                                    stroke="#000"
                                    stroke-width="1.5"
                            >
                                <rect width="30" height="30" rx="8" stroke="none" />
                                <rect
                                        x="0.75"
                                        y="0.75"
                                        width="28.5"
                                        height="28.5"
                                        rx="7.25"
                                        fill="none"
                                />
                            </g>
                            <path
                                    id="Icon_open-question-mark"
                                    data-name="Icon open-question-mark"
                                    d="M5.978,0a6.254,6.254,0,0,0-4.55,1.6A5.281,5.281,0,0,0,0,4.695l2.42.315a2.933,2.933,0,0,1,.75-1.67,3.594,3.594,0,0,1,2.807-.92,4.27,4.27,0,0,1,2.953.823,2.023,2.023,0,0,1,.678,1.6c0,2.009-.823,2.565-2.033,3.63a6.9,6.9,0,0,0-2.807,5.445v.605h2.42v-.605c0-2.009.75-2.565,1.96-3.63a6.94,6.94,0,0,0,2.88-5.445A4.649,4.649,0,0,0,10.6,1.428,6.689,6.689,0,0,0,5.978,0ZM4.768,16.941v2.42h2.42v-2.42Z"
                                    transform="translate(48.986 18.32)"
                                    fill="#000"
                            />
                        </g>
                    </svg>
                </button>
            </div>
            <?php if (has_nav_menu('header-menu-mobile')) wp_nav_menu( array( 'theme_location' => 'header-menu-mobile', 'container_class' => 'container-menu', 'menu_class' => 'ul-res-menu' ) ); ?>

        </div>
    </div>
</header>
<!-- responsive menu -->


