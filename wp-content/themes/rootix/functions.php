<?php
define('ASSETS_DIR', get_template_directory_uri() . '/assets/');
define('STYLES_DIR', ASSETS_DIR . 'styles/');
define('SCRIPTS_DIR', ASSETS_DIR . 'scripts/');
define('IMAGES_DIR', ASSETS_DIR . 'images/');

function dd(...$args)
{
    foreach ($args as $arg) {
        echo '<pre>';
        var_dump($arg);
        echo '</pre>';
    }
    die();
}

// اضافه کردن استایل صفحات
function rootix_register_style()
{
    wp_enqueue_style('owl.carousel.min.css', STYLES_DIR . 'owl.carousel.min.css', array(), '1.0.6');
    wp_enqueue_style('owl.theme.default.min.css', STYLES_DIR . 'owl.theme.default.min.css', array(), '1.0.6');
    wp_enqueue_style('font-awesome.css', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css', array(), '1.0.6');
    wp_enqueue_style('grid.css', STYLES_DIR . 'grid.css', array(), '1.0.6');
    wp_enqueue_style('custom.css', STYLES_DIR . 'custom.css', array(), '1.0.6');
    wp_enqueue_style('app.css', STYLES_DIR . 'app.css', array(), '1.0.6');
    wp_enqueue_style('responsive.css', STYLES_DIR . 'responsive.css', array(), '1.0.6');
    wp_enqueue_style('stylesheet.css', get_stylesheet_uri(), array(), '1.0.6');
    wp_enqueue_style('mr7-custom.css', STYLES_DIR . 'mr7-custom.css', array(), '1.0.6');
}
add_action('wp_enqueue_scripts', 'rootix_register_style');

// اضافه کردن اسکریپت های صفحات
function rootix_register_script()
{
    wp_enqueue_script('Jquery.min.js', SCRIPTS_DIR . 'Jquery.min.js', array(), '1.0.6',true);
    wp_enqueue_script('owl.carousel.min.js', SCRIPTS_DIR . 'owl.carousel.min.js', array('Jquery.min.js'), '1.0.6',true);
    wp_enqueue_script('app.js', SCRIPTS_DIR . 'app.js', array('Jquery.min.js'), '1.0.6',true);
    wp_enqueue_script('filter-slider.js', SCRIPTS_DIR . 'filter-slider.js', array('Jquery.min.js'), '1.0.6',true);
    wp_enqueue_script('blog-categories.js', SCRIPTS_DIR . 'blog-categories.js', array('Jquery.min.js'), '1.0.6',true);

}
add_action('wp_enqueue_scripts', 'rootix_register_script');

// رفع مشکل ریپلی نشدن کامنت ها وقتی افزونه یواست سئو فعال بود
add_filter( 'wpseo_remove_reply_to_com', '__return_false' );

// موارد اضافه شده جهت پشتیبانی قالب
function rootix_theme_support()
{
    add_theme_support('title-tag');
    add_theme_support(
        'post-formats',
        array(
            'link',
            'aside',
            'gallery',
            'image',
            'quote',
            'status',
            'video',
            'audio',
            'chat',
        )
    );
    add_theme_support('automatic-feed-links');
    add_theme_support(
        'custom-background',
        array(
            'default-color' => 'f5efe0',
        )
    );
    add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'rootix_theme_support');

// اضافه کردن سایز تصاویر
function rootix_add_image_size() {
    add_image_size( 'offer-editor-small', 292,192, true );
    add_image_size( 'offer-editor-large', 366, 409, true );
    add_image_size( 'sidebar-small', 55, 55, true );
}
add_action( 'after_setup_theme', 'rootix_add_image_size' );

// اضافه کردن منو برای وردپرس
function register_my_menus()
{
    register_nav_menus(
        array(
            'header-menu' => __('منو بالای صفحات'),
            'header-menu-mobile' => 'منو بالای صفحات موبایل'
        )
    );
}
add_action('init', 'register_my_menus');



require get_template_directory() . '/inc/helper-functions.php';
require get_template_directory() . '/inc/register-widgets.php';
require get_template_directory() . '/cmb2/cmb2.php';
