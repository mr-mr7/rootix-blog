<?php
get_header();
$categories = get_categories([
    'orderby' => 'count',
    'order' => 'desc',
    'parent' => 0,
    'number' => 5,
    'hide_empty' => false,
]);
$tags = get_tags([
    'orderby' => 'count',
    'order' => 'desc',
    'number' => 10,
    'hide_empty' => false,
]);

// پست های پرطرفدار براساس کامنت
$popular_post = get_posts([
    'orderby' => 'comment_count',
    'numberposts' => 3
]);

$editor_suggestion_id = rootixB_get_option('editor_suggestion');
$editor_suggestion_posts = get_posts([
    'numberposts' => 5,
    'include' => explode(',', $editor_suggestion_id)
]);
?>
    <!-- Start Main -->
    <main>
        <?php if (is_home()): ?>
            <!-- Start Section One -->
            <?php get_template_part('template-parts/slider'); ?>
            <!-- End Section One -->

            <?php if (count($editor_suggestion_posts) >= 5): ?>
                <!-- Start Section Two -->
                <section>
                    <div>
                        <div class="blog-title">
                            <div>
                                <h2>پیشنهاد سردبیر</h2>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="container-secondary container-secondary-blog">
                        <div class="row">
                            <div class="col-md-5">
                                <div>
                                    <div class="blog-image-slide-container blog-image-slide-container-2">
                                        <div class="blog-page-top-slide__img teacher__img">
                                            <img src="<?php echo get_the_post_thumbnail_url($editor_suggestion_posts[0], 'offer-editor-large'); ?>"
                                                 alt="<?php echo get_the_title($editor_suggestion_posts[0]) ?>"/>
                                        </div>
                                        <div class="blog-page-top-slide__info">
                                            <div class="blog-page-top-slide__title">
                                                <span><?php the_author_meta('user_nicename', $editor_suggestion_posts[0]->post_author); ?></span>
                                                <span><?php echo $editor_suggestion_posts[0]->comment_count ?> نظر</span>
                                            </div>

                                            <h2><?php echo get_the_title($editor_suggestion_posts[0]) ?></h2>

                                            <a href="<?php echo get_the_permalink($editor_suggestion_posts[0]) ?>">
                                                بیشتر
                                                بخوانید </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="d-flex flex-column justify-between container-left-blog">
                                    <?php
                                    unset($editor_suggestion_posts[0]);
                                    $editor_suggestion_posts = array_merge([], $editor_suggestion_posts);
                                    foreach ($editor_suggestion_posts as $key => $item):
                                        ?>
                                        <?php if (($key + 2) % 2 == 0): ?><div class="d-flex items-center"> <?php endif; ?>
                                        <div>
                                            <div class="blog-image-slide-container blog-image-slide-container-2">
                                                <div class="blog-page-top-slide__img teacher__img">
                                                    <img src="<?php echo get_the_post_thumbnail_url($item, 'offer-editor-small'); ?>"
                                                         alt="<?php echo get_the_title($item) ?>"/>
                                                </div>
                                                <div class="blog-page-top-slide__info">
                                                    <div class="blog-page-top-slide__title">
                                                        <span><?php the_author_meta('user_nicename', $item->post_author); ?></span>
                                                        <span><?php echo $item->comment_count ?> نظر</span>
                                                    </div>
                                                    <h2><?php echo get_the_title($item) ?></h2>
                                                    <a href="<?php echo get_the_permalink($item) ?>"> بیشتر بخوانید </a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if (($key + 2) % 2 != 0): ?></div> <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End Section Two -->
            <?php endif; ?>
        <?php endif; ?>
        <?php if (!is_home()): ?>
            <section>
                <div>
                    <div class="blog-title">
                        <div>
                            <?php if (is_search()): ?>
                                <h2>
                                    جستجو برای:
                                    <span style="color: #0c5460"><?php echo get_search_query(); ?></span>
                                </h2>
                            <?php else: ?>
                                <h2><?php the_archive_title() ?></h2>
                            <?php endif; ?>
                        </div>
                        <hr>
                    </div>
                </div>
            </section>
        <?php endif ?>

        <!-- Start Filter Blog in mobile -->
        <section class="blog-filter">
            <div class="container">
                <div class="search-blog-res">
                    <form action="/" method="get">
                        <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="18.461"
                                height="18.461"
                                viewBox="0 0 18.461 18.461"
                        >
                            <g
                                    id="Icon_feather-search"
                                    data-name="Icon feather-search"
                                    transform="translate(0.5 0.5)"
                                    opacity="1"
                            >
                                <path
                                        id="Path_8461"
                                        data-name="Path 8461"
                                        d="M19.837,12.168A7.668,7.668,0,1,1,12.168,4.5,7.668,7.668,0,0,1,19.837,12.168Z"
                                        transform="translate(-4.5 -4.5)"
                                        fill="none"
                                        stroke="#000"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="1"
                                />
                                <path
                                        id="Path_8462"
                                        data-name="Path 8462"
                                        d="M29.145,29.145l-4.17-4.17"
                                        transform="translate(-11.891 -11.891)"
                                        fill="none"
                                        stroke="#000"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="1"
                                />
                            </g>
                        </svg>
                        <input type="search" name="s" placeholder="جست و جو..."/>
                    </form>
                </div>

                <div class="category-drppdown">

                    <button class="w-100 blog-res-filters-title-btn">
                        <div class="category-icon">
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18.881"
                                    class="plus"
                                    height="18.881"
                                    viewBox="0 0 18.881 18.881"
                            >
                                <g
                                        id="Group_1518"
                                        data-name="Group 1518"
                                        transform="translate(-354.846 -594.737)"
                                >
                                    <path
                                            id="Path_3041"
                                            data-name="Path 3041"
                                            d="M13.185,7.875a.59.59,0,0,1,.59.59v4.72a.59.59,0,0,1-.59.59H8.465a.59.59,0,1,1,0-1.18H12.6V8.465A.59.59,0,0,1,13.185,7.875Z"
                                            transform="translate(351.101 590.992)"
                                            fill="#404040"
                                            fill-rule="evenodd"
                                    />
                                    <path
                                            id="Path_3042"
                                            data-name="Path 3042"
                                            d="M16.875,17.465a.59.59,0,0,1,.59-.59h4.72a.59.59,0,0,1,0,1.18h-4.13v4.13a.59.59,0,1,1-1.18,0Z"
                                            transform="translate(346.821 586.713)"
                                            fill="#404040"
                                            fill-rule="evenodd"
                                    />
                                    <path
                                            id="Path_3043"
                                            data-name="Path 3043"
                                            d="M9.441,17.7A8.261,8.261,0,1,0,1.18,9.441,8.261,8.261,0,0,0,9.441,17.7Zm0,1.18A9.441,9.441,0,1,0,0,9.441,9.441,9.441,0,0,0,9.441,18.881Z"
                                            transform="translate(354.846 594.737)"
                                            fill="#404040"
                                            fill-rule="evenodd"
                                    />
                                </g>
                            </svg>
                        </div>

                        <div class="blog-res-filters__title">
                            <span>دسته بندی ها</span>
                        </div>

                    </button>

                    <ul class="categories">
                        <?php foreach ($categories as $item): ?>
                            <li>
                                <a href="<?php echo get_category_link($item) ?>"
                                   class="w-100 blog-res-filters-title-btn category__dropdown-item">
                                    <?php echo $item->name ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="blog-res-filter">
                    <div class="blog-res-filter__title">
                        <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="19.143"
                                height="16.408"
                                viewBox="0 0 19.143 16.408"
                        >
                            <g id="settings-adjust" transform="translate(-2.25 -4.5)">
                                <path
                                        id="Path_3029"
                                        data-name="Path 3029"
                                        d="M21.393,7.235h-2.8a3.42,3.42,0,0,0-6.7,0H2.25V8.6h9.639a3.42,3.42,0,0,0,6.7,0h2.8ZM15.24,9.969a2.051,2.051,0,1,1,2.051-2.051A2.015,2.015,0,0,1,15.24,9.969Z"
                                        fill="#262c3f"
                                />
                                <path
                                        id="Path_3030"
                                        data-name="Path 3030"
                                        d="M2.25,24.352h2.8a3.42,3.42,0,0,0,6.7,0h9.639V22.985h-9.64a3.42,3.42,0,0,0-6.7,0H2.25ZM8.4,21.617a2.051,2.051,0,1,1-2.051,2.051A2.015,2.015,0,0,1,8.4,21.617Z"
                                        transform="translate(0 -6.179)"
                                        fill="#262c3f"
                                />
                            </g>
                        </svg>
                        <span>تگ ها :</span>
                    </div>

                    <div class="blog-res-filter__btns" style="direction: rtl;">
                        <div class="blog-res-filter__btns-inner">
                            <?php foreach ($tags as $key => $item): if ($key == 8) break ?>
                                <button onclick="window.location='<?php echo get_tag_link($item) ?>'"><?php echo $item->name ?></button>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Filter Blog in mobile -->

        <!-- Start Section Two -->
        <section>
            <div class="container-secondary container-secondary-blog">
                <div class="row blog-row-section-2">
                    <div class="col-12 col-lg-8">
                        <div class="right-blog-page">
                            <?php if (have_posts()): ?>
                                <?php
                                while (have_posts()) {
                                    the_post();
                                    get_template_part('template-parts/post-item');
                                }
                                ?>
                                <?php get_template_part('template-parts/pagination'); ?>
                            <?php else: ?>
                                <div class="alert alert-warning">موردی جهت نمایش وجود ندارد</div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-12 col-lg-4" dir="ltr">
                        <div class="left-blog-page">
                            <div class="blog-search">
                                <h3>جست و جو</h3>
                                <form action="/" method="get" class="left-blog-page__search-form">
                                    <input type="search" name="s" id="search" placeholder="جست و جو..."/>
                                    <button type="submit">
                                        <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="18.461"
                                                height="18.461"
                                                viewBox="0 0 18.461 18.461"
                                        >
                                            <g
                                                    id="Icon_feather-search"
                                                    data-name="Icon feather-search"
                                                    transform="translate(0.5 0.5)"
                                                    opacity="0.6"
                                            >
                                                <path
                                                        id="Path_8461"
                                                        data-name="Path 8461"
                                                        d="M19.837,12.168A7.668,7.668,0,1,1,12.168,4.5,7.668,7.668,0,0,1,19.837,12.168Z"
                                                        transform="translate(-4.5 -4.5)"
                                                        fill="none"
                                                        stroke="#000"
                                                        stroke-linecap="round"
                                                        stroke-linejoin="round"
                                                        stroke-width="1"
                                                />
                                                <path
                                                        id="Path_8462"
                                                        data-name="Path 8462"
                                                        d="M29.145,29.145l-4.17-4.17"
                                                        transform="translate(-11.891 -11.891)"
                                                        fill="none"
                                                        stroke="#000"
                                                        stroke-linecap="round"
                                                        stroke-linejoin="round"
                                                        stroke-width="1"
                                                />
                                            </g>
                                        </svg>
                                    </button>
                                </form>
                            </div>

                            <div class="side-news-container">
                                <h3 class="left-blog-page__title">اخبار پرطرفدار</h3>
                                <?php foreach ($popular_post as $item): ?>
                                    <?php get_template_part('template-parts/post-item-side', '', ['item' => $item]); ?>
                                <?php endforeach; ?>
                            </div>

                            <div class="categories-blog">
                                <h3 class="left-blog-page__title">دسته بندی</h3>
                                <ul class="categories-blog__menu">
                                    <?php foreach ($categories as $item): ?>
                                        <li class="categories-blog__item">
                                            <div class="circle-blog-page"></div>
                                            <a href="<?php echo get_category_link($item) ?>"
                                               class="categories-blog__link">
                                                <?php echo $item->name ?>
                                                <span><?php echo $item->count ?></span>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>

                            <div class="blog-tags">
                                <h3 class="left-blog-page__title">تگ ها</h3>
                                <div class="blog-tags__items">
                                    <?php foreach ($tags as $item): ?>
                                        <a href="<?php echo get_tag_link($item) ?>"
                                           class="blog-tags__item"><?php echo $item->name ?>,</a>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <div class="newsletters-container">
                                <h3 class="left-blog-page__title">خبرنامه آنلاین</h3>
                                <div class="newsletters">
                                    <div class="newsletters__desc">
                                        <p>برای دریافت خبرنامه های روتیکس</p>
                                        <p>ایمیل خود را وارد کنید و به روز باشید</p>
                                    </div>
                                    <?php echo do_shortcode('[mailpoet_form id="1"]') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section Two -->

    </main>
    <!-- End Main Content -->
<?php
get_footer();
?>