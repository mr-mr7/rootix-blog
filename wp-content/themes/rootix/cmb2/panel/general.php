<?php
add_action( 'cmb2_admin_init', 'rootixB_register_theme_options_metabox' );
function rootixB_register_theme_options_metabox() {

    $cmb_options = new_cmb2_box( array(
        'id'           => 'rootixB_option_metabox',
        'title'        => esc_html__( 'Site Options', 'rootixB' ),
        'object_types' => array( 'options-page' ),

        /*
         * The following parameters are specific to the options-page box
         * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
         */

        'option_key'      => 'rootixB_options', // The option key and admin menu page slug.
        // 'icon_url'        => 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
        // 'menu_title'      => esc_html__( 'Options', 'rootixB' ), // Falls back to 'title' (above).
        // 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
        // 'capability'      => 'manage_options', // Cap required to view options-page.
        // 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
        // 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
        // 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
        // 'save_button'     => esc_html__( 'Save Theme Options', 'rootixB' ), // The text for the options-page save button. Defaults to 'Save'.
    ) );
    $cmb_options->add_field( array(
        'name' => "پیشنهاد سردبیر",
        'desc' => "در اینجا ایدی پست های مربوطه رو گذاشته و با , از هم جدا نمایید مثال: 1,2,3 (حتما 5 ایدی وارد کنید)",
        'id'   => 'editor_suggestion',
        'type' => 'text',
    ) );

}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string $key     Options array key
 * @param  mixed  $default Optional default value
 * @return mixed           Option value
 */
function rootixB_get_option( $key = '', $default = false ) {
    if ( function_exists( 'cmb2_get_option' ) ) {
        // Use cmb2_get_option as it passes through some key filters.
        return cmb2_get_option( 'rootixB_options', $key, $default );
    }

    // Fallback to get_option if CMB2 is not loaded yet.
    $opts = get_option( 'rootixB_options', $default );

    $val = $default;

    if ( 'all' == $key ) {
        $val = $opts;
    } elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
        $val = $opts[ $key ];
    }

    return $val;
}
